### Automated quality control of T1-weighted brain MRI scans for clinical research datasets: methods comparison and design of a quality prediction classifier

---

Preprint of the full paper is available [here](https://www.medrxiv.org/content/10.1101/2024.04.12.24305603v1)

---

<p style='text-align: justify;'> These codes are written to design automated T1w brain MRI quality prediction classifiers. The workflow involves processing T1w images using two existing tools (MRIQC and CAT12). The feature vectors are then designed and used as an input to the nested cross validation framework (repeated multiple times). The target labels are visual QC binary ratings (0 and 1). The training is done with 80% of the total data and 20% data was kept as test data. The final model was created by extracting the hyperparameters and features from the best performing models in nested cross validation iterations. The classification performance was calculated on test data. Additionally, leave-one-site out codes are also provided. In this, each site was retained as test data and the models were trained on the data from remaining sites.

---
Tools required: 
1. [MRIQC](https://mriqc.readthedocs.io/en/latest/) (singularity container provided)
2. [CAT12](https://neuro-jena.github.io/cat12-help/) ([standalone version](https://neuro-jena.github.io/enigma-cat12/#standalone); also needs MATLAB runtime compiler)
3. [MATLAB](https://uk.mathworks.com/products/matlab.html) 2023a (System Identification Toolbox, Statistics and Machine Learning Toolbox, Parallel Computing Toolbox)

---
Classifiers:

1. [Support vector machine](https://uk.mathworks.com/help/stats/fitcsvm.html) 
2. [Random forest](https://uk.mathworks.com/help/stats/fitcensemble.html)
3. [Random undersampling boost](https://uk.mathworks.com/help/stats/fitcensemble.html)
---

**QC measures calculation**

1. Run MRIQC pipeline\
`singularity run mriqc-0.15.1.simg <input directory> <output directory> participant --participant_label <participant id> -m T1w --verbose-reports --no-sub --n_procs 4 --mem_gb 8`

2. Run MRIQC group report\
`singularity run mriqc-0.15.1.simg <input directory> <output directory> group`

2. Run MRIQC random forest classifier\
`singularity exec mriqc-0.15.1.simg mriqc_clf --load-classifier -X group_T1w.tsv`

4. Run CAT12 segmentation\
`cat_standalone.sh -m MATLAB/MATLAB_Compiler_Runtime/v93 -b cat_standalone_segment_enigma.m T1.nii.gz`

---
**Prepare for classification**

* *featurenames.csv*     : Order of features in subjects-by-features matrix (subjects = rows; features = columns)

---
**Classification**

* *batch_main.m*         : Define parameters to run nested cross validation
* *main.m*               : Train and test classifiers 
* *get_parameters.m*     : Get the features and hyperparameters for final model
* *final_model.m*        : Create a final model from the parameters  

---
**Dependent files/codes**

* *function_main*        : Contains are the functions required to run main.m
* *hyperparameter_files* : Hyperparameters used for optimisation

---
**Additional analyses**

* *LOO_models*                                  :  Codes provided to run leave-one-site-out models 
* *Model variability and statistical analysis*  :  Codes provided to run model variability and statistical analysis on CV and test data performances

---

**Trained model** [Random undersampling boost with top 80 ranked features]

* *trainedMdl.mat*       : Trained model to predict QC for new data\
`predicted_labels = predict(trainedMdl_RUS,Test_data(:,ranked_features(1:80,1)))`\
`[acc,sens,spec,balacc,prec,f1] = calc_confusionmat_measures(Test_target,predicted_labels,classNames)`

**Trained models including motion artefacts dataset** [includes training data from the elderly cohorts and young healthy population (MR-ART)]

* For more details on the MR-ART data [See here](https://openneuro.org/datasets/ds004173/versions/1.0.2)
* Three trained models are provided : models inc. 1%, 60% and 80% of the MR-ART data in addition to the elderly datasets in the main paper (N = 1955)
* For more details on this analysis. See 'Supplementary materials of the paper' in this repository

---
**Supplementary materials of the paper**

* *supplementary_materials*  : Contains supplementary materials of the paper


