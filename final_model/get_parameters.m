%% Load data
data_dir = 'work_dir';
mydir    = fullfile(data_dir,'nested_CV_dir');
files    = dir(fullfile(mydir,'run*.mat'));

%% Get best hp

data_svm  = cell(0);
data_rf   = cell(0);
data_rus  = cell(0);

for file = 1:length(files)
    fname    = fullfile(files(file).folder,files(file).name);
    load(fname, 'hp_save_outer_svm' , 'hp_save_outer_rf', 'hp_save_outer_rus');    
    data_svm = [data_svm;hp_save_outer_svm(:,1)];
    data_rf  = [data_rf;hp_save_outer_rf(:,1)];
    data_rus = [data_rus;hp_save_outer_rus(:,1)];
end
% Get feature-wise HP
data_temp   = horzcat(data_svm{:});
hp1_temp    = data_temp(:,1:3:end);
hp2_temp    = data_temp(:,2:3:end);
hp3_temp    = data_temp(:,3:3:end);
hp1_mode    = mode(cell2mat(hp1_temp),2);
hp2_mode    = mode(cell2mat(hp2_temp),2);
hp3_mode    = cell(size(hp3_temp,1),1);
val1 = {'linear','rbf'};
for jj = 1:size(hp3_temp,1)
    tmp  = hp3_temp(jj,:);
    tmp2 = sum(ismember(tmp,val1{1,1}));
    tmp3 = sum(ismember(tmp,val1{1,2}));
    if tmp2>tmp3
        hp3_mode{jj,1} = val1{1,1};
    else
        hp3_mode{jj,1} = val1{1,2};
    end
end
best_hp_svm = [num2cell([hp1_mode,hp2_mode]),hp3_mode];
clear hp1_temp hp2_temp hp3_temp hp1_mode hp2_mode hp3_mode data_temp

data_temp   = cell2mat(data_rf');
hp1_temp    = data_temp(:,1:2:end);
hp2_temp    = data_temp(:,2:2:end);
hp1_mode    = mode(hp1_temp,2);
hp2_mode    = mode(hp2_temp,2);
best_hp_rf = [hp1_mode,hp2_mode];
clear hp1_temp hp2_temp hp1_mode hp2_mode data_temp

data_temp   = cell2mat(data_rus');
hp1_temp    = data_temp(:,1:3:end);
hp2_temp    = data_temp(:,2:3:end);
hp3_temp    = data_temp(:,3:3:end);
hp1_mode    = mode(hp1_temp,2);
hp2_mode    = mode(hp2_temp,2);
hp3_mode    = mode(hp3_temp,2);
best_hp_rus = [hp1_mode,hp2_mode,hp3_mode];
clear hp1_temp hp2_temp hp2_temp hp1_mode hp2_mode hp3_mode data_temp

save(fullfile(mydir,'best_hp_per_feature_svm.mat'),'best_hp_svm');
save(fullfile(mydir,'best_hp_per_feature_rf.mat'),'best_hp_rf');
save(fullfile(mydir,'best_hp_per_feature_rus.mat'),'best_hp_rus');


%% Rank features
RankedFeatures = cell(1,length(files));

for ii = 1:length(files)
    fname                 = fullfile(files(ii).folder,files(ii).name);
    load(fname,'Outer_selected_features')
    [aggR, ~, rowNames]   = aggregateRanks(Outer_selected_features);
    agg_fR                = [aggR,rowNames];
    rankedFeatures        = sortrows(agg_fR,1);
    RankedFeatures{:,ii}  = rankedFeatures(:,2);
end

[aggR, ~, rowNames]   = aggregateRanks(RankedFeatures);
agg_fR                = [aggR,rowNames];
rankedFeatures        = sortrows(agg_fR,1);
final_ranked          = rankedFeatures(:,2);
save(fullfile('final_ranked.mat'),'final_ranked');