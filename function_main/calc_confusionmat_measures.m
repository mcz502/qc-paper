%% Confusion matrix measures for classifier performance for two class classifier
% Calculate sensitivity (recall), specificity, accuracy, balanced accuracy, precision, F1 score

function [Accuracy,Sensitivity,Specificity,Balanced_acc,Precision,F1_score]=calc_confusionmat_measures(targets,prediction,classNames)

    cp     =   confusionmat(targets,prediction,'order',classNames);

    TP     =   cp(1,1); 
    FP     =   cp(2,1);
    FN     =   cp(1,2);
    TN     =   cp(2,2);

    Accuracy     =  ((TP + TN) / (TP + TN + FP + FN))*100; % overall accuracy

    Sensitivity  =  (TP / (TP + FN))*100; % recall

    Specificity  =  (TN / (TN + FP))*100; % specificity

    Balanced_acc =  ((Sensitivity + Specificity) / 2); % balanced accuracy

    Precision    =  (TP / (TP + FP))*100;

    F1_score     =  ((2 * Precision * Sensitivity) / (Precision + Sensitivity));
end
