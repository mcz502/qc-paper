function RAR_RankedFeatures=RobustAggRanking(train_data,train_target)
    [~,f2]=size(train_data); % f2 is feature vector length

    % Perform fscna feature selection on nested CV fold
%     nca = fscnca(train_data,train_target,'FitMethod','exact','Solver','sgd',...
%                     'Standardize',false,'Verbose',1);
%     nca_fweights=[(1:1:f2)',nca.FeatureWeights];
%     sort_nca_fweights=sortrows(nca_fweights,-2);

    % Rank importance of predictors using ReliefF or RReliefF algorithm
    k_relief=10;
    [fidx_relief,~]=relieff(train_data,train_target,k_relief);
    fidx_relief=fidx_relief';
    
    % Univariate feature ranking for classification using chi-square tests
    chi_idx=fscchi2(train_data,train_target);
    chi_idx=chi_idx';
    
    % Rank features for classification using minimum redundancy maximum relevance (MRMR) algorithm
    mrmr_idx=fscmrmr(train_data,train_target);
    mrmr_idx=mrmr_idx';
    
    % Rank key features by class separability criteria
    
    % 1) ttest : Absolute value two-sample t-test with pooled variance estimate
    ttest_idx=rankfeatures(train_data',train_target,'Criterion','ttest');
    
    % 2) entropy : Relative entropy, also known as Kullback-Leibler distance or divergence
    entropy_idx=rankfeatures(train_data',train_target,'Criterion','entropy');
    
    % 3) bhattacharya : Minimum attainable classification error or Chernoff bound
    bhatta_idx=rankfeatures(train_data',train_target,'Criterion','bhattacharyya');
    
    % 4) wilcoxon : Absolute value of the standardized u-statistic of a two-sample ...
    %               unpaired Wilcoxon test, also known as Mann-Whitney
    wilcox_idx=rankfeatures(train_data',train_target,'Criterion','wilcoxon');
    
    % 5) roc : Area between the empirical receiver operating characteristic (ROC) ...
    %          curve and the random classifier slope
    roc_idx=rankfeatures(train_data',train_target,'Criterion','roc');
    

    %% aggregate ranking
    fs_allMethods     = cell(1,8);
%     fs_allMethods{1,1}= sort_nca_fweights(:,1);
    fs_allMethods{1,1} = fidx_relief;
    fs_allMethods{1,2} = chi_idx;
    fs_allMethods{1,3} = mrmr_idx;
    fs_allMethods{1,4} = ttest_idx;
    fs_allMethods{1,5} = entropy_idx;
    fs_allMethods{1,6} = bhatta_idx;
    fs_allMethods{1,7} = wilcox_idx;
    fs_allMethods{1,8} = roc_idx;

    [aggR, ~, rowNames] = aggregateRanks(fs_allMethods);
    agg_fR=[aggR,rowNames];
    RAR_RankedFeatures=sortrows(agg_fR,1);
end


%% Rank with weights
% % function rank_fs_1=rankweights(fs_rank)
% %     [f3,f4]=size(fs_rank);    
% %     for i=1:f3
% %         fs_rank(i,2)=i/f3;
% %     end
% %     rank_fs_1=zeros(f3,1);
% %     f_num=fs_rank(:,1);
% %     f_wts=fs_rank(:,2);
% %     for j=1:f3
% %         rank_fs_1(f_num(j,1),1)=f_wts(j,1);
% %     end
% % end

%%
