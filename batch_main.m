% Use this script to run the main.m.
% Define parameters in this script
workdir = pwd

hyperparafilepath_svm  = fullfile('hyperparameter_files','grSearch_svm.mat');
hyperparafilepath_rf   = fullfile('hyperparameter_files','grSearch_rf.mat');
hyperparafilepath_rus  = fullfile('hyperparameter_files','grSearch_rus.mat');

datamat           = 'train_data.mat'; % subjects in rows; features in columns
outfolds          = 5;
infolds           = 3;
featurestart      = 'cjv'; % name of the first feature
ratingsCol        = 'NewManual'; % visual QC ratings (binary)
siteCol           = 'Scanner'; % column name with scanner information
runClassifier     = 1; % if 0: the classifier will only perform feature ranking
featureset        = [10,20,30,40,50,60,70,80,90,100,104]'; % feature sizes

outpath           = 'output_dir'; % path to output directory where 'run/s' will be stored

if ~exist(outpath,'dir')
    mkdir(outpath)
end

n = 50; % number of repeats for nested cross validation

% Using parallel computing toolbox in MATLAB 
parfor mul_runs = 1:n
    main(hyperparafilepath_svm,hyperparafilepath_rf,hyperparafilepath_rus,datamat,outfolds,infolds,featurestart,featureset,ratingsCol,siteCol,runClassifier,outpath,mul_runs)
end
