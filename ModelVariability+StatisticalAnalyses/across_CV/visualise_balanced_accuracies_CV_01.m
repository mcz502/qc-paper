%% Visualise balanced accuracies across 100 runs of cross validation for each feature size

% load the data
data          = load('input_vars.mat');
cat12_mdls    = data.CV_cat12_rus_perf;
mriqc_mdls    = data.CV_mriqc_rus_perf;
combined_mdls = data.CV_combined_rus_perf;

% Boxplots across 100 runs for each feature size
f = figure;
f.Position(3:4) = [800,400];
boxplot([cat12_mdls,mriqc_mdls,combined_mdls(:,1:end)])

hold on; xline(4.5,'-.r'); xline(11.5,'-.r')

xticklabels({'cat12-10','cat12-20','cat12-30','cat12-36*',...
             'mriqc-10', 'mriqc-20','mriqc-30','mriqc-40','mriqc-50','mriqc-60','mriqc-68*',...
              'comb-10','comb-20','com-30','comb-40','comb-50','comb-60','comb-70','comb-80*','comb-90','comb-100','comb-104'})  
box('off')
grid('on')
xlabel('Feature Sets');
ylabel('Balanced Accuracy (%)');
