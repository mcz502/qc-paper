% load data
data          = load('input_vars.mat');
cat12_mdls    = data.CV_cat12_rus_perf;
mriqc_mdls    = data.CV_mriqc_rus_perf;
combined_mdls = data.CV_combined_rus_perf;


%% statistically compare for reported feature sizes: kstest h=1 for all so doing non-parameteric tests
cat12_36    = cat12_mdls(:,end)';
mriqc_68    = mriqc_mdls(:,end)';
comb_80     = combined_mdls(:,8)'; % top 80 was reported

% Combine data into matrix
data = [cat12_36; mriqc_68; comb_80]';
groups = [1 2 3]; % 1 = cat12, 2 = mriqc, 3 = comb

% 1) run non-parametric tests: Friedman and then wilconxon for pairwise
[p,tbl,stats] = friedman(data, 1);

fprintf('Friedman test: p = %.4f\n', p);

% Post-hoc comparisons (if significant Friedman result)
if p < 0.05
    fprintf('Post-hoc tests needed (use pairwise Wilcoxon tests).\n');
end

[p1,h1,stats1] = signrank(cat12_36, mriqc_68); % cat12 vs. mriqc
[p2,h2,stats2] = signrank(comb_80, mriqc_68);  % mriqc vs. comb
[p3,h3,stats3] = signrank(comb_80, cat12_36);  % cat12 vs. comb

% Display results
fprintf('Wilcoxon signed-rank test results:\n');
fprintf('cat12 vs. mriqc: p = %.4f\n', p1);
fprintf('mriqc vs. comb: p = %.4f\n', p2);
fprintf('cat12 vs. comb: p = %.4f\n', p3);

cols           = {'cat12_vs_mriqc', 'comb_vs_mriqc','comb_cat12'}';
results_wilcox = [h1, p1, stats1.zval, stats1.signedrank;h2, p2, stats2.zval, stats2.signedrank;h3, p3, stats3.zval, stats3.signedrank];
tab_results_wilcox = cell2table([cols,num2cell(results_wilcox)]);
tab_results_wilcox.Properties.VariableNames = {'comparison','h','p','zval','signedrank'};

