%% Bootstrap analysis

% load the data
data          = load('input_vars.mat');
cat12_mdls    = data.CV_cat12_rus_perf;
mriqc_mdls    = data.CV_mriqc_rus_perf;
combined_mdls = data.CV_combined_rus_perf;

% feature sizes for each model
featureSizes_cat12 = [10, 20, 30, 36];
featureSizes_mriqc = [10, 20, 30, 40, 50, 60, 68];
featureSizes_comb  = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 104];

% Balanced accuracies 
balancedAccuracies_cat12 = cat12_mdls'; % Example data
balancedAccuracies_mriqc = mriqc_mdls'; % Example data
balancedAccuracies_comb  = combined_mdls'; % Example data

% Number of bootstrap samples
numBootstrapSamples = 10000;

meanCI_cat12 = zeros(4,2);
stdCI_cat12  = zeros(4,2);
meanCI_mriqc = zeros(7,2);
stdCI_mriqc  = zeros(7,2);
meanCI_comb  = zeros(11,2);
stdCI_comb   = zeros(11,2);

% Calculate confidence intervals for each feature size
for i = 1:length(featureSizes_cat12)
    [meanCI_cat12(i,:), stdCI_cat12(i,:)] = bootstrapCI(balancedAccuracies_cat12(i, :), numBootstrapSamples);
    fprintf('cat12_%d: Mean CI = [%f, %f], Std CI = %f\n', featureSizes_cat12(i), meanCI_cat12(i,1), meanCI_cat12(i,2), stdCI_cat12(i,1));
end

for i = 1:length(featureSizes_mriqc)
    [meanCI_mriqc(i,:), stdCI_mriqc(i,:)] = bootstrapCI(balancedAccuracies_mriqc(i, :), numBootstrapSamples);
    fprintf('mriqc_%d: Mean CI = [%f, %f], Std CI = %f\n', featureSizes_mriqc(i), meanCI_mriqc(i,1), meanCI_mriqc(i,2), stdCI_mriqc(i,1));
end

for i = 1:length(featureSizes_comb)
    [meanCI_comb(i,:), stdCI_comb(i,:)] = bootstrapCI(balancedAccuracies_comb(i, :), numBootstrapSamples);
    fprintf('comb_%d: Mean CI = [%f, %f], Std CI = %f\n', featureSizes_comb(i), meanCI_comb(i,1), meanCI_comb(i,2), stdCI_comb(i,1));
end
% Function to perform bootstrapping and calculate confidence intervals
function [meanCI, stdCI] = bootstrapCI(data, numBootstrapSamples)
    rng(42)
    n = length(data);
    bootstrapMeans = zeros(numBootstrapSamples, 1);
    
    for i = 1:numBootstrapSamples
        % Resample with replacement using randsample
        bootstrapSample = data(randsample(n, n, true));
        % Calculate the mean of the bootstrap sample
        bootstrapMeans(i) = mean(bootstrapSample);
    end
    
    % Calculate 95% confidence intervals
    meanCI = prctile(bootstrapMeans, [2.5 97.5]);
    stdCI = std(bootstrapMeans);
end
