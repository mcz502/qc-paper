%% Get bootstrap CI and mean balanced accuracies on the test data

data = load('input_vars.mat');

y_true            = data.testdata_ground_truth_qc;
y_rus_comb_seedp  = data.testdata_comb_rus_perf(:,8);
y_rus_cat12_seedp = data.testdata_cat12_rus_perf(:,end);
y_rus_mriqc_seedp = data.testdata_mriqc_rus_perf(:,end);

[meanBA_comb, CIL_comb, CIU_comb, balacc_comb]     = bootstrap_test(y_true,y_rus_comb_seedp,10000);
[meanBA_cat12, CIL_cat12, CIU_cat12, balacc_cat12] = bootstrap_test(y_true,y_rus_cat12_seedp,10000);
[meanBA_mriqc, CIL_mriqc, CIU_mriqc, balacc_mriqc] = bootstrap_test(y_true,y_rus_mriqc_seedp,10000);

% Wilcoxon pairwise test
[p_wilc_combined_CAT12, h_combined_CAT12, stats_combined_CAT12] = signrank(balacc_comb, balacc_cat12);
[p_wilc_combined_MRIQC, h_combined_MRIQC, stats_combined_MRIQC] = signrank(balacc_comb, balacc_mriqc);
[p_wilc_CAT12_MRIQC, h_CAT12_MRIQC, stats_CAT12_MRIQC]          = signrank(balacc_cat12, balacc_mriqc);

fprintf('Wilcoxon P-value (Combined vs. CAT12): %.15f\n', p_wilc_combined_CAT12);
fprintf('Wilcoxon P-value (Combined vs. MRIQC): %.15f\n', p_wilc_combined_MRIQC);
fprintf('Wilcoxon P-value (CAT12 vs. MRIQC): %.15f\n', p_wilc_CAT12_MRIQC);


%% MC-Nemar test for error_rates

% error rate calculation
comb_error_rate = (sum(y_rus_comb_seedp ~= y_true)/483) .* 100;
mriqc_error_rate = (sum(y_rus_mriqc_seedp ~= y_true)/483) .* 100;
cat12_error_rate = (sum(y_rus_cat12_seedp ~= y_true)/483) .* 100;

fprintf('Misclassification rate (Combined): %.15f\n', comb_error_rate);
fprintf('Misclassification rate (MRIQC-only): %.15f\n', mriqc_error_rate);
fprintf('Misclassification rate (CAT12-only): %.15f\n', cat12_error_rate);


% do test
[chi2_stat_comb_cat12, p_value_comb_cat12] = do_mcnemarTests(y_true,y_rus_comb_seedp,y_rus_cat12_seedp);
[chi2_stat_comb_mriqc, p_value_comb_mriqc] = do_mcnemarTests(y_true,y_rus_comb_seedp,y_rus_mriqc_seedp);
[chi2_stat_cat12_mriqc, p_value_cat12_mriqc] = do_mcnemarTests(y_true,y_rus_cat12_seedp,y_rus_mriqc_seedp);

%%
function [meanBA, CI_lower, CI_upper,balancedAccuracies] = bootstrap_test(y_true,y_pred,B)
    rng(42)
    balancedAccuracies = zeros(B, 1);
    % Bootstrapping
    for i = 1:B
        % Resample indices with replacement
        idx = randsample(length(y_true), length(y_true), true);
        
        % Get resampled data
        y_true_boot = y_true(idx);
        y_pred_boot = y_pred(idx);
        
        % Compute Balanced Accuracy
        C = confusionmat(y_true_boot, y_pred_boot);
        Sensitivity = C(2,2) / sum(C(2,:)); % Recall for class 1
        Specificity = C(1,1) / sum(C(1,:)); % Recall for class 0
        balancedAccuracies(i) = (Sensitivity + Specificity) / 2;
    end
    % Compute 95% Confidence Interval
    CI_lower = prctile(balancedAccuracies, 2.5);
    CI_upper = prctile(balancedAccuracies, 97.5);
    meanBA = mean(balancedAccuracies);
    
    % Display Results
    fprintf('Balanced Accuracy: %.3f (95%% CI: %.3f - %.3f)\n', meanBA, CI_lower, CI_upper);
end

%%
function [chi2_stat, p_value] = do_mcnemarTests(y_true,y_pred_A,y_pred_B)
    % Compute disagreement counts
    n_01 = sum((y_pred_A == y_true) & (y_pred_B ~= y_true)); % A correct, B wrong
    n_10 = sum((y_pred_A ~= y_true) & (y_pred_B == y_true)); % A wrong, B correct
    
    % McNemar's Test Statistic
    chi2_stat = (abs(n_01 - n_10) - 1)^2 / (n_01 + n_10);
    
    % Compute p-value using chi-square distribution
    p_value = 1 - chi2cdf(chi2_stat, 1);
    
    % Display result
    fprintf('McNemar Test: Chi2: %.5f, P-value: %.5f\n', chi2_stat, p_value);
    
    % Interpretation: If p < 0.05, the models have significantly different error rates.
end
