%% Final model performance comparison across feature sizes
% For seed preserved: test data comparison for RUS classifier (final model) 
% - CAT12 only, MRIQC only, Combined  

comb_dir      = '/vols/Data/pipelines-DPUK/QC_classifier_additionalMeasures/Mixed_data/Dec2023_new_rf_svm_rus/Aug2024_seed_same';
comb_rus      = load(fullfile(comb_dir,'final_model_vars.mat'),'perf_rus_tab');
comb_rus_vals = comb_rus.perf_rus_tab;

cat12_dir      = '/vols/Data/pipelines-DPUK/QC_classifier_additionalMeasures/Mixed_data/Aug2024_Tool_only_models/CAT12_models/Aug2024_seed_same';
cat12_rus      = load(fullfile(cat12_dir,'final_model_vars.mat'),'perf_rus_tab');
cat12_rus_vals = cat12_rus.perf_rus_tab;

mriqc_dir      = '/vols/Data/pipelines-DPUK/QC_classifier_additionalMeasures/Mixed_data/Aug2024_Tool_only_models/MRIQC_models/Aug2024_seed_same';
mriqc_rus      = load(fullfile(mriqc_dir,'final_model_vars.mat'),'perf_rus_tab');
mriqc_rus_vals = mriqc_rus.perf_rus_tab;


comb_bal_acc  = comb_rus_vals.BalancedAcc;
cat12_bal_acc = cat12_rus_vals.BalancedAcc;
mriqc_bal_acc = mriqc_rus_vals.BalancedAcc;

% Define x-values for each dataset
comb_x = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 104];
cat12_x = [10, 20, 30, 36];
mriqc_x = [10, 20, 30, 40, 50, 60, 68];

% Define y-values (Balanced Accuracy) for each dataset
comb_y = comb_bal_acc;
cat12_y = cat12_bal_acc;
mriqc_y = mriqc_bal_acc;

% Create the plot
f = figure;
f.Position(3:4) = [800, 500];
hold on;
grid on;
box off;

% Plot each dataset
plot(comb_x, comb_y, 'Color', [49 163 84]./255, 'Marker', 'o', 'LineWidth', 1.5, 'MarkerSize', 8, 'DisplayName', 'Combined');
plot(cat12_x, cat12_y, 'Color', [230 85 13]./255, 'Marker', 'o', 'LineWidth', 1.5, 'MarkerSize', 8, 'DisplayName', 'CAT12');
plot(mriqc_x, mriqc_y, 'Color', [117 107 177]./255, 'Marker','o', 'LineWidth', 1.5, 'MarkerSize', 8, 'DisplayName', 'MRIQC');

% Add color-coded vertical lines

xline(36, '--', 'Color', [230 85 13]./255,'LineWidth', 1.5, 'DisplayName', 'CAT12 - all features'); % Light blue
xline(68, '--', 'Color', [117 107 177]./255, 'LineWidth', 1.5, 'DisplayName', 'MRIQC - all features'); % Light red
xline(80, '--', 'Color', [49 163 84]./255, 'LineWidth', 1.5, 'DisplayName', 'Combined - reported'); % Light green

% Add labels, legend, and title
xlabel('Feature Size');
ylabel('Balanced Accuracy (%)');
ylim([70, 90]); % Set y-axis limits
xticks(comb_x)
xticklabels({'10','20','30','40','50','60','70','80','90','100','104'})
xtickangle(45)
set(gca,'fontsize',12)
legend('Location', 'bestoutside'); % Automatically generates a legend
saveas(gcf,'final_mdl_featurewise_variability.fig')
print('final_mdl_featurewise_variability','-dpng','-r900')
