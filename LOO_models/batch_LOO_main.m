% Initialize variables, paths   
mydir                 = 'site_dir';
hyperparafilepath_svm = 'grSearch_svm.mat';
hyperparafilepath_rf  = 'grSearch_rf.mat';
hyperparafilepath_rus = 'grSearch_rus.mat';

datamat               = fullfile(mydir,'train_test_data.mat');
outpath               = fullfile(mydir,'output_dir');

if ~exist(outpath,'dir')
    mkdir(outpath)
end

featurestart          = 'cjv';
ratingsCol            = 'NewManual';
siteCol               = 'Scanner';
featureset            = [10,20,30,40,50,60,70,80,90,100,104]';
runClassifier         = 1;
run_no                = 1;

% Run
LOO_main(hyperparafilepath_svm,hyperparafilepath_rf,hyperparafilepath_rus, datamat,featurestart,featureset,ratingsCol,siteCol,outpath,runClassifier,run_no)