%% Load data
data_dir = 'work_dir';
mydir    = fullfile(data_dir,'site');
load(fullfile(mydir,'train_test_data.mat'));
load(fullfile(mydir,'nested_CV_dir','run_1.mat'),'hp_save_outer_svm','hp_save_outer_rf','hp_save_outer_rus','Outer_selected_features');

%% Get best hp: SVM

best_hp_svm  = cell(length(hp_save_outer_svm),3);
best_hp_rf   = zeros(length(hp_save_outer_rf),2);
best_hp_rus  = zeros(length(hp_save_outer_rus),3);

for features = 1:length(hp_save_outer_svm)
    temp_hp                 = hp_save_outer_svm(:,features);
    temp_hp1                = vertcat(temp_hp{:});
    best_hp_svm{features,1} = mode(cell2mat(temp_hp1(:,1)));
    best_hp_svm{features,2} = mode(cell2mat(temp_hp1(:,2)));

    val1     = {'linear','rbf'};
    hp3_temp = temp_hp1(:,3);
    for jj = 1:size(hp3_temp,1)
        tmp  = hp3_temp(jj,:);
        tmp2 = sum(ismember(tmp,val1{1,1}));
        tmp3 = sum(ismember(tmp,val1{1,2}));
        if tmp2>tmp3
            best_hp_svm{features,3} = val1{1,1};
        else
            best_hp_svm{features,3} = val1{1,2};
        end
    end

    clear temp_hp temp_hp1

    temp_hp                 = hp_save_outer_rf(:,features);
    temp_hp1                = cell2mat(temp_hp);
    best_hp_rf(features,:)  = mode(temp_hp1);
    clear temp_hp temp_hp1

    temp_hp                 = hp_save_outer_rus(:,features);
    temp_hp1                = cell2mat(temp_hp);
    best_hp_rus(features,:) = mode(temp_hp1);
    clear temp_hp temp_hp1
end

save(fullfile(mydir,'nested_CV_dir','best_hp_per_feature_svm.mat'),'best_hp_svm');
save(fullfile(mydir,'nested_CV_dir','best_hp_per_feature_rf.mat'), 'best_hp_rf');
save(fullfile(mydir,'nested_CV_dir','best_hp_per_feature_rus.mat'),'best_hp_rus');

%% Rank features

[aggR, ~, rowNames]   = aggregateRanks(Outer_selected_features);
agg_fR                = [aggR,rowNames];
rankedFeatures        = sortrows(agg_fR,1);
final_ranked          = rankedFeatures(:,2);
save(fullfile(mydir,'nested_CV_dir','final_ranked.mat'),'final_ranked');