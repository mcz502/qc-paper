%% This version doesnt do site specific scaling of features
function LOO_final_model(maindir,comb_mdl_para_dir,train_data_dir,scanner_omit)
    cd(maindir)
    mydir   = fullfile(maindir,'nested_CV_dir');
    
    load(fullfile(comb_mdl_para_dir,'best_hp_per_feature_svm.mat'),'best_hp_svm');
    load(fullfile(comb_mdl_para_dir,'best_hp_per_feature_rf.mat'),'best_hp_rf');
    load(fullfile(comb_mdl_para_dir,'best_hp_per_feature_rus.mat'),'best_hp_rus');
    
    load(fullfile(comb_mdl_para_dir,'final_ranked.mat'),'final_ranked');
    
    featureset = [10,20,30,40,50,60,70,80,90,100,104]';
    
    %% Train final model, use best hp
    
    % Load data
    data_dir = maindir;
    load(fullfile(data_dir,'train_test_data.mat'),'test_site');
    
    
    % Train classifier with best HP
    featurestart   = 'cjv';
    ratingsCol     = 'NewManual';
    siteCol        = 'Scanner';
    
    load(fullfile(train_data_dir,'train_test_data.mat'),'all_train');
    train_site = all_train(~ismember(all_train.Scanner,scanner_omit),:);

    % assign train test
    gtrain = train_site;
    gtest  = test_site;
    
    classNames  = unique(gtrain.(ratingsCol));
    classNames  = classNames';     % Set the classes here 
    Sidx        = find(string(gtrain.Properties.VariableNames) == featurestart);
    NumFeatures = length(featureset);
    
    % Get column ids for feature start, rating and site
    Sidx           = find(string(gtrain.Properties.VariableNames) == featurestart);
    Ridx           = find(string(gtrain.Properties.VariableNames) == ratingsCol);
    
    train_data   = table2array(gtrain(:,Sidx:end));
    test_data    = table2array(gtest(:,Sidx:end));
    train_target = table2array(gtrain(:,Ridx));
    test_target  = table2array(gtest(:,Ridx));
    
    [tmp_train,tmp_test] = scale_train_test(train_data,test_data);
    
    train_data_target   = [train_target, tmp_train];
    test_data_target    = [test_target, tmp_test];
    rng(1)
    shuffled_train      =  train_data_target(randperm(size(train_data_target,1)),:);
    
    sTrain_data         = shuffled_train(:,2:end);
    sTrain_target       = shuffled_train(:,1);
    Test_data           = test_data_target(:,2:end);
    Test_target         = test_data_target(:,1);
    
    ranked_features     = final_ranked(:,1);
    
    perf_svm = zeros(6,NumFeatures);
    perf_rf  = zeros(6,NumFeatures);
    perf_rus = zeros(6,NumFeatures);
    
    mdl_svm  = cell(NumFeatures,1);
    mdl_rf   = cell(NumFeatures,1);
    mdl_rus  = cell(NumFeatures,1);
    
    prediction_svm = zeros(length(Test_target),NumFeatures);
    prediction_rf  = zeros(length(Test_target),NumFeatures);
    prediction_rus = zeros(length(Test_target),NumFeatures);
    
    for i3 = 1:NumFeatures
        features = sTrain_data(:,ranked_features(1:featureset(i3,1),1));
        
        mdl_svm{i3,1}        = fitcsvm(features,sTrain_target,'BoxConstraint',best_hp_svm{i3,1},'KernelFunction',best_hp_svm{i3,3},'KernelScale',best_hp_svm{i3,2});
        prediction_svm(:,i3) = predict(mdl_svm{i3,1},Test_data(:,ranked_features(1:featureset(i3,1),1)));
    
        t_rf                 = templateTree('MaxNumSplits',best_hp_rf(i3,1));
        mdl_rf{i3,1}         = fitcensemble(features,sTrain_target,'Method','Bag','Learners',t_rf,'NumLearningCycles',best_hp_rf(i3,2));  
        prediction_rf(:,i3)  = predict(mdl_rf{i3,1},Test_data(:,ranked_features(1:featureset(i3,1),1)));
    
        t_rus                = templateTree('MaxNumSplits',best_hp_rus(i3,1));
        mdl_rus{i3,1}        = fitcensemble(features,sTrain_target,'Method','RUSBoost','Learners',t_rus,'NumLearningCycles',best_hp_rus(i3,2),'LearnRate',best_hp_rus(i3,3));
        prediction_rus(:,i3)  = predict(mdl_rus{i3,1},Test_data(:,ranked_features(1:featureset(i3,1),1)));
    
        [perf_svm(1,i3),perf_svm(2,i3),...
         perf_svm(3,i3),perf_svm(4,i3),...
         perf_svm(5,i3),perf_svm(6,i3)]...
            = calc_confusionmat_measures(Test_target,prediction_svm(:,i3),classNames);
         
        [perf_rf(1,i3),perf_rf(2,i3),...
         perf_rf(3,i3),perf_rf(4,i3),...
         perf_rf(5,i3),perf_rf(6,i3)]...
            = calc_confusionmat_measures(Test_target,prediction_rf(:,i3),classNames);
    
        [perf_rus(1,i3),perf_rus(2,i3),...
         perf_rus(3,i3),perf_rus(4,i3),...
         perf_rus(5,i3),perf_rus(6,i3)]...
            = calc_confusionmat_measures(Test_target,prediction_rus(:,i3),classNames);
                           
    end
    
    perf_svm_tab = array2table(perf_svm');
    perf_svm_tab.Properties.VariableNames = {'Accuracy', 'Sensitivity','Specificity',...
                                              'BalancedAcc', 'Precision','F1score'};
    perf_svm_tab.Features    = [1:1:NumFeatures]';
    perf_svm_tab             = movevars(perf_svm_tab,'Features','Before','Accuracy');
    
    perf_rf_tab = array2table(perf_rf');
    perf_rf_tab.Properties.VariableNames = {'Accuracy', 'Sensitivity','Specificity',...
                                              'BalancedAcc', 'Precision','F1score'};
    perf_rf_tab.Features    = [1:1:NumFeatures]';
    perf_rf_tab             = movevars(perf_rf_tab,'Features','Before','Accuracy');
    
    perf_rus_tab                          = array2table(perf_rus');
    perf_rus_tab.Properties.VariableNames = {'Accuracy', 'Sensitivity','Specificity',...
                                              'BalancedAcc', 'Precision','F1score'};
    perf_rus_tab.Features                 = [1:1:NumFeatures]';
    perf_rus_tab                          = movevars(perf_rus_tab,'Features','Before','Accuracy');
    
    
    save(fullfile('final_model_vars.mat'))
end 

